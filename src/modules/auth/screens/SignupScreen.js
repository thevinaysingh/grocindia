import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  ToastAndroid,
} from 'react-native';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Container, Content, Card, Item, Input, Icon, Button } from 'native-base';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Colors, dimensions, labelStyles, containerStyles } from '../../../themes';
import { Header, StatusBar } from '../../../components';
import { LoginManager } from '../../../firebase';
import {
  linkState,
  isEmailValid,
  isPasswordValid,
  isDisplayNameValid,
  isAddressValid,
  isPincodeValid,
  isPhoneValid,
  focusOnNext,
} from '../../../utils';
import { errors } from '../../../constants';

const styles = {
  keyBoardTextContainer: {
    height: 40,
    backgroundColor: Colors.defaultGreyColor,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  keyBoardText: {
    fontSize: 16,
    paddingHorizontal: 10,
    color: Colors.themeIconColor,
  },
  iconStyle: {
    color: Colors.themeIconColor,
  },
};

class Signup extends Component {
  constructor() {
    super();

    this.state = {
      displayName: '',
      email: '',
      mob: '',
      password: '',
      address: '',
      landmark: '',
      city: '',
      state: '',
      pincode: '',
      isKeyboardOpen: false,
      isLoading: false,
    };
  }

  handleSubmit = () => {
    Keyboard.dismiss();
    const {
      email,
      mob,
      password,
      address,
      landmark,
      pincode,
      displayName,
      city,
      state,
    } = this.state;
    if (!isDisplayNameValid(displayName)) {
      Alert.alert(errors.nameErrorText);
      return;
    } else if (!isEmailValid(email)) {
      Alert.alert(errors.emailErrorText);
      return;
    } else if (!isPhoneValid(mob)) {
      Alert.alert(errors.mobErrorText);
      return;
    } else if (!isPasswordValid(password)) {
      Alert.alert(errors.passwordErrorText);
      return;
    } else if (!isAddressValid(address)) {
      Alert.alert(errors.addressErrorText);
      return;
    } else if (!isAddressValid(city)) {
      Alert.alert(errors.cityErrorText);
      return;
    } else if (!isAddressValid(state)) {
      Alert.alert(errors.stateErrorText);
      return;
    } else if (!isPincodeValid(pincode)) {
      Alert.alert(errors.pincodeErrorText);
      return;
    }
    this.setState({ isLoading: true });
    LoginManager.signUpRequest(this.state)
    .then((response) => {
      this.setState({ 
        isLoading: false,
      });
      ToastAndroid.show('Successfully registered!', ToastAndroid.LONG);
      Actions.pop();
    }).catch((error) => {
      this.setState({ 
        isLoading: false,
      });
      Alert.alert('Registration Error', `${error}`);
    });
  }

  handleBack = () => {
    Keyboard.dismiss();
    Actions.pop();
  }

  focusInput(inputField) {
    this[inputField]._root.focus();
  }

  renderFooterOption = () => {
    return (
      <View
        style={styles.keyBoardTextContainer}>
        <Text
          onPress={this.handleBack}
          style={[styles.keyBoardText, { paddingLeft: 20 }]}
        >Back</Text>
        <Text
          onPress={() => Keyboard.dismiss()}
          style={[styles.keyBoardText, { right: 100, position: 'absolute' }]}
        >Cancel</Text>
        <Text
          onPress={this.handleSubmit}
          style={[styles.keyBoardText, { right: 20, position: 'absolute' }]}
        >Register</Text>
      </View>
    );
  }

  render() {
    return (
      <Container style={containerStyles.defaultContainer}>
        <StatusBar />
        <Header
          title="Register"
          showMenu={false}
          onPressleftIcon={() => Actions.pop()}
        />
        <Content contentContainerStyle={{ padding: dimensions.defaultDimension }}>
          <Card style={{ padding: dimensions.defaultDimension }}>
            <Text style={labelStyles.blackMediumLabel}>Personal Detail</Text>
            <Item
              style={{
                marginBottom: dimensions.smallDimension,
                marginTop: -dimensions.smallDimension,
              }}
            >
              <Icon style={styles.iconStyle} active name='person' />
              <Input
                style={labelStyles.blackSmallLabel}
                placeholderTextColor={Colors.placeholderTxtColor}
                placeholder={'First & last name'}
                returnKeyType={'next'}
                getRef={(ref) => { this.nameInput = ref; }}
                onSubmitEditing={() => this.focusInput('emailInput')}
                {...linkState(this, 'displayName')}
              />
            </Item>
            <Item style={{ marginVertical: dimensions.smallDimension }}>
              <Icon style={styles.iconStyle} active name='mail' />
              <Input
                ref={(ref) => { this.emailInput = ref; }}
                style={labelStyles.blackSmallLabel}
                placeholderTextColor={Colors.placeholderTxtColor}
                placeholder={'Email'}
                keyboardType={'email-address'}
                returnKeyType={'next'}
                onSubmitEditing={() => this.focusInput('mobileInput')}
                {...linkState(this, 'email')}
              />
            </Item>
            <Item style={{ marginVertical: dimensions.smallDimension }}>
              <Icon style={styles.iconStyle} active name='keypad' />
              <Input
                ref={(ref) => { this.mobileInput = ref; }}
                style={labelStyles.blackSmallLabel}
                placeholderTextColor={Colors.placeholderTxtColor}
                placeholder={'Mobile'}
                keyboardType={'phone-pad'}
                returnKeyType={'next'}
                maxLength={10}
                onSubmitEditing={() => this.focusInput('passwordInput')}
                {...linkState(this, 'mob')}
              />
            </Item>
            <Item style={{ marginVertical: dimensions.smallDimension }}>
              <Icon style={styles.iconStyle} active name='lock' />
              <Input
                ref={(ref) => { this.passwordInput = ref; }}
                style={labelStyles.blackSmallLabel}
                placeholderTextColor={Colors.placeholderTxtColor}
                placeholder={'Password'}
                returnKeyType="next"
                secureTextEntry
                onSubmitEditing={() => this.focusInput('addressInput')}
                {...linkState(this, 'password')}
              />
            </Item>
            <Text style={[labelStyles.blackMediumLabel, { marginTop: 15 }]}>Current Location</Text>
            <Item style={{ marginVertical: dimensions.smallDimension, alignItems: 'flex-start' }}>
              <Icon style={{ color: Colors.themeIconColor, paddingTop: 10 }} active name='paper' />
              <Input
                ref={(ref) => { this.addressInput = ref; }}
                style={labelStyles.blackSmallLabel}
                placeholderTextColor={Colors.placeholderTxtColor}
                placeholder={'Address'}
                onSubmitEditing={() => this.focusInput('landmarkInput')}
                {...linkState(this, 'address')}
              />
            </Item>
            <Item style={{ marginVertical: dimensions.smallDimension }}>
              <Icon style={styles.iconStyle} active name='navigate' />
              <Input
                ref={(ref) => { this.landmarkInput = ref; }}
                style={labelStyles.blackSmallLabel}
                placeholderTextColor={Colors.placeholderTxtColor}
                placeholder={'Landmark'}
                returnKeyType="next"
                onSubmitEditing={() => this.focusInput('cityInput')}
                {...linkState(this, 'landmark')}
              />
            </Item>
            <Item style={{ marginVertical: dimensions.smallDimension }}>
              <Icon style={styles.iconStyle} active name='rose' />
              <Input
                ref={(ref) => { this.cityInput = ref; }}
                style={labelStyles.blackSmallLabel}
                placeholderTextColor={Colors.placeholderTxtColor}
                placeholder={'City'}
                returnKeyType="next"
                onSubmitEditing={() => this.focusInput('stateInput')}
                {...linkState(this, 'city')}
              />
            </Item>
            <Item style={{ marginVertical: dimensions.smallDimension }}>
              <Icon style={styles.iconStyle} active name='paw' />
              <Input
                ref={(ref) => { this.stateInput = ref; }}
                style={labelStyles.blackSmallLabel}
                placeholderTextColor={Colors.placeholderTxtColor}
                placeholder={'State'}
                returnKeyType="next"
                onSubmitEditing={() => this.focusInput('pincodeInput')}
                {...linkState(this, 'state')}
              />
            </Item>
            <Item style={{ marginVertical: dimensions.smallDimension }}>
              <Icon style={styles.iconStyle} active name='calculator' />
              <Input
                ref={(ref) => { this.pincodeInput = ref; }}
                style={labelStyles.blackSmallLabel}
                placeholderTextColor={Colors.placeholderTxtColor}
                placeholder={'Pincode'}
                keyboardType={'numeric'}
                returnKeyType="done"
                maxLength={6}
                onSubmitEditing={() => this.handleSubmit}
                {...linkState(this, 'pincode')}
              />
            </Item>
            {this.state.isLoading ?
            <ActivityIndicator
              animating={Boolean(true)}
              color={'#bc2b78'}
              size={'large'}
              style={containerStyles.activityIndicator}
            /> :
            <Button
              onPress={() => this.handleSubmit()}
              full
              style={{ backgroundColor: Colors.primaryBgColor, marginVertical: 15 }}
            >
              <Text style={labelStyles.primaryButtonLabel}>REGISTER</Text>
            </Button>}
          </Card>
          <View style={containerStyles.rowCenteredContainer}>
            <Text style={labelStyles.blackMediumLabel}> {"Already have an account?"} </Text>
            <TouchableOpacity onPress={() => Actions.pop()}>
              <Text style={labelStyles.linkLabelStyle}>Login</Text>
            </TouchableOpacity>
          </View>
        </Content>        
      </Container>
    );
  }
}

Signup.propTypes = {
  toggleDrawer: PropTypes.func,
};

Signup.defaultProps = {
  toggleDrawer: _.noop,
};

const mapStateToProps = state => ({});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
