import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  ListView,
  RefreshControl,
  TouchableOpacity,
  Text,
  ActivityIndicator,
  Alert,
  NetInfo,
  ToastAndroid,
} from 'react-native';
import { Container, Item, Input } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { containerStyles, dimensions, Colors, labelStyles } from '../../../themes';
import { Header, StatusBar } from '../../../components';
import { CategoryListItem } from '../components';
import { local } from '../../../constants';

const styles = StyleSheet.create({
  listHeader: {
    marginTop: 5,
    padding: 10,
    backgroundColor: Colors.primaryBgColor,
    color: 'white',
    fontSize: dimensions.primayFontSize,
  },
  content: {
    alignSelf: 'stretch',
    flex: 1,
    padding: 5,
  },
  iconStyle: {
    color: Colors.themeIconColor,
    marginRight: 5,
  },
  filterIconStyle: {
    color: Colors.placeholderTxtColor,
    marginRight: 5,
  },
  filterIconStyle2: {
    color: Colors.primaryBgColor,
  },
  filterIconStyle2Container: {
    marginTop: 10,
    height: 40,
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  suggestedTextContainer: {
    padding: 10,
  },
  listContainerStyle: {
    height: dimensions.getViewportHeight() / 3,
    width: dimensions.getViewportWidth() - 30,
  },
  searchBar: {
    margin: 10,
    paddingLeft: 10,
    borderWidth: 1,
    borderColor: Colors.placeholderTxtColor,
    borderRadius: 5,
    flexDirection: 'row',
    height: 40,
    alignItems: 'center',
  },
  trueSearchBar: {
    borderWidth: 1,
    borderColor: Colors.blackIconColor,
    marginLeft: 10,
    marginTop: 10,
  },
});

class AdminHome extends Component {
  constructor() {
    super();
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      dataSource: ds.cloneWithRows(local.newCategoryList),
      refreshing: false,
      isLoading: false,
    };
  }

  componentDidMount() {
    // TODO: Load list of categories.
    // this.loadCategories();
  }

  componentWillReceiveProps(newProps) {
    console.log('newProps ==', newProps);
  }

  // loadCategories = () => {
  //   this.setState({ isLoading: true });
  //   networkConnectivity().then(() => {
  //     FirebaseManager.loadAccounts()
  //     .then((accounts) => {
  //       const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
  //       this.setState({
  //         isLoading: false,
  //         refreshing: false,
  //         accounts: accounts,
  //         dataSource: ds.cloneWithRows(accounts),
  //       });
  //     }).catch((error) => {
  //       this.setState({ isLoading: false, refreshing: false });
  //       Alert.alert('Load account error', `${error}`);
  //     });
  //   }).catch((error) => {
  //     const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
  //     this.setState({
  //       isLoading: false,
  //       refreshing: false,
  //       accounts: FirebaseManager.accounts,
  //       dataSource: ds.cloneWithRows(FirebaseManager.accounts),
  //     });
  //     Alert.alert('Network Error', `${error}`);
  //   });
  // }

  // onPopupEvent = (eventName, index, category) => {
  //   // TODO:
  //   if (eventName !== 'itemSelected') return
  //   if (index === 0) {
  //     // Redirect to category form to edit the category.
  //     Actions.categoryForm({ create: false, category: category});
  //   } else if(index === 1) {
  //     Alert.alert('Delete category', 'Are you sure you want to delete the category',[
  //       {
  //         text: 'NO',
  //         style: 'cancel',
  //       },{
  //         text: `I'M Sure`,
  //         onPress: () => this.handleDeletecategory(category),
  //       }
  //     ]);
  //   }
  // }

  // handleDeleteCategory = (category) => {
  //   // Delete the category.
  //   // FirebaseManager.deleteAccount(account)
  //   // .then(() => {
  //   //   ToastAndroid.show('Successfully deleted!', ToastAndroid.LONG);
  //   //   this.loadAccounts();
  //   // }).catch((error) => {
  //   //   Alert.alert('Error', `${error}`);
  //   // });
  // }

  // handleRefresh = () => {
  //   TODO:
  //   this.setState({refreshing: true});
  //   this.loadAccounts();
  // }

  render() {
    const {
      refreshing,
      isLoading,
    } = this.state;
    return (
      <Container style={containerStyles.defaultContainer}>
        <StatusBar />
        <Header
          title="Admin Home"
          onPressleftIcon={() => { /* WIP */ }}
          showAddIcon
          onPressAdd={() => Actions.categoryForm({
            create: true,
          })}
        />
        <View style={styles.content}>
          {isLoading ?
            <ActivityIndicator
              animating={Boolean(true)}
              color={'#bc2b78'}
              size={'large'}
              style={containerStyles.activityIndicator}
            /> :
            <ListView
              style={{ marginVertical: 5 }}
              enableEmptySections
              dataSource={this.state.dataSource}
              renderRow={category => (
                <CategoryListItem
                  key={category.key}
                  onPopupEvent={this.onPopupEvent}
                  category={category}
                />)}
              refreshControl={
                <RefreshControl
                  refreshing={refreshing}
                  // onRefresh={this.handleRefresh}
                />
              }
            />}
        </View>
      </Container>
    );
  }
}

AdminHome.propTypes = {
  toggleDrawer: PropTypes.func,
};

AdminHome.defaultProps = {
  toggleDrawer: undefined,
};

const mapStateToProps = state => ({});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(AdminHome);
