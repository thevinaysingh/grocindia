import React, { Component } from 'react';
import {
  Alert,
  ActivityIndicator,
  ToastAndroid,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Feather';
import { Container, Content, Card, Item, Input, Button, Text } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { Colors, dimensions, labelStyles, containerStyles } from '../../../themes';
import { Header, StatusBar } from '../../../components';
import {
  linkState,
  isEmailValid,
  isAddressValid,
  isPhoneValid,
  assignRef,
  focusOnNext,
} from '../../../utils';

const styles = {
  iconStyle: {
    color: Colors.themeIconColor,
    marginRight: 5,
  },
  iconEyeStyle: {
    color: Colors.whiteIconColor,
    width: 30,
    height: 30,
    alignSelf: 'center',
    borderRadius: 5,
    textAlign: 'center',
    textAlignVertical: 'center',
    backgroundColor: Colors.primaryBgColor,
  },
  addToCartButton: {
    backgroundColor: 'transparent',
    alignSelf: 'flex-end',
    borderRadius: 5,
    margin: 5,
  },
  addToCartButtonText: {
    ...labelStyles.cancelButtonLabel,
    paddingHorizontal: 10,
    paddingVertical: 5,
    color: Colors.themeIconColor,
  },
};

class CategoryForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categoryName: '',
      subCategoryName: '',
      categorySubName: '',
      subCategorySubName: '',
      showAddSubCategoryForm: false,
    };
  }

  render() {
    const isSubmitDisabled = false;
    return (
      <Container style={containerStyles.defaultContainer}>
        <StatusBar />
        <Header
          title={this.props.create ? 'Add Category' : 'Edit Category'}
          onPressleftIcon={() => Actions.adminHomeScreen({ type: 'reset' })}
          showMenu={false}
        />
        <Content contentContainerStyle={{ padding: dimensions.defaultDimension }}>
          <Card style={{ padding: dimensions.defaultDimension }}>
            <Item
              style={{
                marginBottom: dimensions.smallDimension,
                marginTop: -dimensions.smallDimension,
              }}
            >
              <Icon style={styles.iconStyle} size={20} name='briefcase' />
              <Input
                style={labelStyles.blackSmallLabel}
                placeholderTextColor={Colors.placeholderTxtColor}
                placeholder={'Category name'}
                returnKeyType={'next'}
                onSubmitEditing={() => focusOnNext(this, 'categorySubNameInput')}
                {...linkState(this, 'categoryName')}
              />
            </Item>
            <Item style={{ marginVertical: dimensions.smallDimension }}>
              <Icon style={styles.iconStyle} size={20} name='briefcase' />
              <Input
                ref={ref => assignRef(this, 'categorySubNameInput', ref)}
                style={labelStyles.blackSmallLabel}
                placeholderTextColor={Colors.placeholderTxtColor}
                placeholder={'Subname'}
                returnKeyType={'done'}
                {...linkState(this, 'categorySubName')}
              />
            </Item>
            {this.state.showAddSubCategoryForm ?
              <Card style={{ padding: dimensions.defaultDimension }}>
                <Text>Create New Category</Text>
                <Item
                  style={{
                    marginBottom: dimensions.smallDimension,
                    marginTop: -dimensions.smallDimension,
                  }}
                >
                  <Icon style={styles.iconStyle} size={20} name='briefcase' />
                  <Input
                    style={labelStyles.blackSmallLabel}
                    placeholderTextColor={Colors.placeholderTxtColor}
                    placeholder={'Sub Category name'}
                    returnKeyType={'next'}
                    onSubmitEditing={() => focusOnNext(this, 'subCategorySubNameInput')}
                    {...linkState(this, 'subCategoryName')}
                  />
                </Item>
                <Item style={{ marginVertical: dimensions.smallDimension }}>
                  <Icon style={styles.iconStyle} size={20} name='briefcase' />
                  <Input
                    ref={ref => assignRef(this, 'subCategorySubNameInput', ref)}
                    style={labelStyles.blackSmallLabel}
                    placeholderTextColor={Colors.placeholderTxtColor}
                    placeholder={'Subname'}
                    returnKeyType={'done'}
                    {...linkState(this, 'subCategorySubName')}
                  />
                </Item>
                <TouchableOpacity
                  style={styles.addToCartButton}
                  onPress={() => this.setState({ showAddSubCategoryForm: false })}
                >
                  <Text style={styles.addToCartButtonText}>Create</Text>
                </TouchableOpacity>
              </Card> :
              <TouchableOpacity
                onPress={() => this.setState({ showAddSubCategoryForm: true })}
                style={{ marginVertical: 10 }}
              >
                <Text style={labelStyles.themeIconColor}> + Create New Sub category</Text>
              </TouchableOpacity>}
            {this.state.isLoading ?
              <ActivityIndicator
                animating={Boolean(true)}
                color={'#bc2b78'}
                size={'large'}
                style={containerStyles.activityIndicator}
              /> :
              <Button
                onPress={() => this.handleSubmit()}
                full
                disabled={isSubmitDisabled}
                style={{
                  backgroundColor: isSubmitDisabled ?
                    Colors.placeholderTxtColor : Colors.primaryBgColor,
                  marginVertical: 15,
                }}
              >
                <Text style={labelStyles.primaryButtonLabel}>{this.props.create ? 'Add' : 'Edit'}</Text>
              </Button>}
          </Card>
        </Content>
      </Container>
    );
  }
}

CategoryForm.propTypes = {
  account: PropTypes.any,
  create: PropTypes.bool,
};

CategoryForm.defaultProps = {
  account: {},
  create: true,
};

const mapStateToProps = state => ({});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(CategoryForm);
