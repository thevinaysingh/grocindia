import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Feather';
import { Card, CardItem } from 'native-base';
import _ from 'lodash';
import {
  Text,
  StyleSheet,
  View,
  Image,
} from 'react-native';
import { Colors, labelStyles } from '../../../themes';
import PopupMenu from './PopupMenu';

const styles = StyleSheet.create({
  container: {
    alignItems: 'flex-start',
    backgroundColor: Colors.defaultBgColor,
    padding: 5,
    flexDirection: 'row',
    marginHorizontal: 10,
    marginVertical: 5,
    borderRadius: 5,
    elevation: 10,
  },
  leftContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    alignSelf: 'stretch',
  },
  iconStyle: {
    color: Colors.themeIconColor,
  },
  titleTextStyle: {
    color: Colors.themeIconColor,
    fontSize: 17,
  },
  textStyle: {
    color: 'grey',
    fontSize: 15,
    paddingVertical: 2,
  },
  categoryImage: {
    height: 60,
    width: 60,
    borderRadius: 10,
    backgroundColor: 'transparent',
  },
  separator: {
    height: 1,
    backgroundColor: 'transparent',
    flex: 1,
    paddingRight: 10,
  },
});

class CategoryListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showSubcategoryList: false,
    };
  }

  renderSubCategory() {
    return (
      <View>
        <Text>List of sub catgories</Text>
        {this.props.category.sub_category.map((subCategory) => {
          return (
            <CardItem
              key={subCategory.key}
              style={{ borderWidth: 1, borderColor: 'grey', marginTop: 5 }}
            >
              <View style={styles.leftContainer}>
                <Text style={labelStyles.blackMediumLabel}>{subCategory.name}</Text>
              </View>
              <Icon
                name="chevron-right"
                color="grey"
                size={20}
                onPress={() => alert('Sub category' + subCategory.key + subCategory.name)}
              />
            </CardItem>
          );
        })}
      </View>
    );
  }

  render() {
    const { category } = this.props;
    const { showSubcategoryList } = this.state;
    return (
      <Card style={{ padding: 5 }}>
        <View style={{ alignItems: 'flex-start', flexDirection: 'row' }}>
          <View style={styles.leftContainer}>
            <Text style={labelStyles.blackLargeLabel}>{category.name}</Text>
            <Text style={labelStyles.blackMediumLabel}>{category.sub_name}</Text>
          </View>
          <Image source={{ uri: category.category_image }} style={styles.categoryImage} />
          <PopupMenu
            actions={['Edit', 'Delete']}
            onPress={(eventName, index) => this.props.onPopupEvent(eventName, index, category)}
          />
        </View>
        <View style={{ flexDirection: 'row', paddingVertical: 5, alignItems: 'center' }}>
          <View style={styles.separator} />
          {showSubcategoryList ?
            <Icon
              onPress={() => this.setState({ showSubcategoryList: false })}
              name="chevron-up"
              color="grey"
              size={25}
            /> :
            <Icon
              onPress={() => this.setState({ showSubcategoryList: true })}
              name="chevron-down"
              color="grey"
              size={25}
            />
          }
        </View>
        {showSubcategoryList && this.renderSubCategory()}

      </Card>
    );
  }
}

CategoryListItem.propTypes = {
  category: PropTypes.any,
  onPressItem: PropTypes.func,
  onPopupEvent: PropTypes.func,
};

CategoryListItem.defaultProps = {
  category: {},
  onPressItem: _.noop,
  onPopupEvent: _.noop,
};

export default CategoryListItem;
