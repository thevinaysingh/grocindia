export const unAuthenticatedMenuList = [
  {
    label: 'Home',
    key: 'home',
    icon: 'home',
    active: false,
  },
  {
    label: 'About us',
    key: 'about',
    icon: 'info',
    active: false,
  },
  {
    label: 'Your profile',
    key: 'profile',
    icon: 'user',
    active: false,
  },
  {
    label: 'Your cart',
    key: 'cart',
    icon: 'shopping-cart',
    active: false,
  },
  {
    label: 'Your order',
    key: 'order',
    icon: 'anchor',
    active: false,
  },
  {
    label: 'Your kirana',
    key: 'kirana',
    icon: 'briefcase',
    active: false,
  },
  {
    label: 'Settings',
    key: 'settings',
    icon: 'settings',
    active: false,
  },
  {
    label: 'Contact us',
    key: 'contact',
    icon: 'phone',
    active: false,
  },
  {
    label: 'Privacy & Policy',
    key: 'terms',
    icon: 'file-text',
    active: false,
  },
  {
    label: 'Feedback',
    key: 'feedback',
    icon: 'clipboard',
    active: false,
  },
];
export const authenticatedMenuList = [
  {
    label: 'Home',
    key: 'home',
    active: false,
  },
  {
    label: 'My profile',
    key: 'profile',
    active: false,
  },
  {
    label: 'My cart',
    key: 'cart',
    active: false,
  },
  {
    label: 'My order',
    key: 'order',
    active: false,
  },
  {
    label: 'My kirana',
    key: 'kirana',
    active: false,
  },
  {
    label: 'Settings',
    key: 'settings',
    active: false,
  },
  {
    label: 'Contact us',
    key: 'contact',
    active: false,
  },
  {
    label: 'Terms & conditions',
    key: 'terms',
    active: false,
  },
  {
    label: 'Feedback',
    key: 'feedback',
    active: false,
  },
  {
    label: 'Share',
    key: 'share',
    active: false,
  },
  {
    label: 'Logout',
    key: 'logout',
    active: false,
  },
];
