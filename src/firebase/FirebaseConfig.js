import * as firebase from 'firebase';

export class FirebaseConfig {
  /**
   * Initialise Firebase
   */
  static initialise() {
    if (!firebase.apps.length) {
      firebase.initializeApp({
        apiKey: "AIzaSyCpvYNcEHOFbwc8SDJEVAQ9VnGZK4y21q4",
        authDomain: "groc-india.firebaseapp.com",
        databaseURL: "https://groc-india.firebaseio.com",
        projectId: "groc-india",
        storageBucket: "groc-india.appspot.com",
        messagingSenderId: "12199272300"
      });
    }
  }
}
