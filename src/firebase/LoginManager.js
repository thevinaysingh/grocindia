import React, { Component } from 'react';
import {
  Alert,
} from 'react-native';
import { FirebaseManager } from './FirebaseManager';

export class LoginManager {

  /* Login request flow completed */
  static loginRequest = (email, password) => {
    return FirebaseManager.getAuthRef().signInWithEmailAndPassword(email, password)
    .then((response) => {
      FirebaseManager.uid = response.uid;
      return Promise.resolve();
    }).catch((error) => {
      return Promise.reject(error);
    });  
  };

  /* Signup request flow completed */
  static signUpRequest = (request) => {
    return FirebaseManager.getAuthRef().createUserWithEmailAndPassword(request.email, request.password)
    .then((response) => {
      const newUser = {
        profile: {
          display_name: request.displayName,
          email: request.email,
          mobile: request.mob,
          password: request.password,
          address: request.address,
          city: request.city,
          state: request.state,
          landmark: request.landmark,
          pincode: request.pincode,
        },
        items: {
          title: 'USER_ITEM_DATABASE',
        }
      }
      FirebaseManager.getDBRef().ref('users/'+ response.uid).set(newUser);
      return Promise.resolve(response);
    }).catch((error) => {
      return Promise.reject(error);
    });
  };

  /* TODO: */
  static resetPassword = (email) => {
    
  };

  /* TODO: */
  static changePassword = (oldPassword, newPassword) => {
    
  };

  /* Signout flow completed */
  static logout = () => {
    return FirebaseManager.getAuthRef().signOut()
    .then(() => {
      FirebaseManager.uid = '';
      FirebaseManager.profile = {};
      FirebaseManager.items = [];
      return Promise.resolve();
    }).catch((error) => {
      return Promise.reject(error);
    });  
  };
}